using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TMP_Text paddle1ScoreText;
    [SerializeField] private TMP_Text paddle2ScoreText;
    [SerializeField] private Transform paddle1Transform;
    [SerializeField] private Transform paddle2Transform;
    [SerializeField] private Transform ballTransform;
    private int paddle1Score;
    private int paddle2Score;
    public GameObject menuInicio;
    public GameObject contenido;
    public GameObject menuWinner1;
    public GameObject menuWinner2;
    private static GameManager instance;
    
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            contenido.SetActive(true);
            menuInicio.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        if(paddle1Score == 10)
        {
            menuWinner1.SetActive(true);
            contenido.SetActive(false);
        
        if (Input.GetKeyDown(KeyCode.X))
        {
            contenido.SetActive(true);
            menuWinner1.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        }
            if(paddle2Score == 10)
        {
            menuWinner2.SetActive(true);
            contenido.SetActive(false);
        
        if (Input.GetKeyDown(KeyCode.X))
        {
            contenido.SetActive(true);
            menuWinner2.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        }
    }

    // METODOS PUBLICOS PARA ACCEDERLOS DESDE LOS OTROS SCRIPTS
    public void Paddle1Scored()
    {
        paddle1Score++;
        paddle1ScoreText.text = paddle1Score.ToString();
    }
    public void Paddle2Scored()
    {
        paddle2Score++;
        paddle2ScoreText.text = paddle2Score.ToString();
    }
    public void Restart()
    {
        paddle1Transform.position = new Vector2(paddle1Transform.position.x, 0);
        paddle2Transform.position = new Vector2(paddle2Transform.position.x, 0);
        ballTransform.position = new Vector2(0, 0);
    }
}
